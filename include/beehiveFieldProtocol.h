/*
 * beehiveFieldProtocol.h
 *
 *  Created on: Aug 24, 2015
 *      Author: sherpa
 */

#ifndef INCLUDE_BEEHIVEFIELDPROTOCOL_H_
#define INCLUDE_BEEHIVEFIELDPROTOCOL_H_



#include <stdint.h>



#define MAX3(a,b,c) (((a)>(b)?(a):(b))>(c)?((a)>(b)?(a):(b)):(c))



#define NB_MAX_RETRY 3



enum serviceId_e
{
	logOn                    = 0x10 ,
	logOn_Ack                = 0x11 ,

	beehiveDataFrame         = 0x20 ,
	beehiveDataFrame_Ack     = 0x21 ,

	collectorDataFrame       = 0x30 ,
	collectorDataFrame_Ack   = 0x31 ,

	logOff                   = 0x70 ,
	logOff_Ack               = 0x71 ,

	beehiveDataReadRequest   = 0x80 ,
	beehiveDataReadResponse  = 0x81 ,

	beehiveDataWriteRequest  = 0x90 ,
	beehiveDataWriteResponse = 0x91 ,

} ;



typedef uint8_t frameSize_t ;
typedef uint8_t serviceId_t ;

typedef uint32_t unitId_t ;

typedef unitId_t beehiveId_t ;
typedef unitId_t collectorId_t ;

typedef uint16_t value_t ;

typedef uint16_t nbMsg_t ;

typedef uint32_t sessionKey_t ;

typedef uint16_t protocolVersion_t ;

typedef uint8_t errorCode_t ;
typedef uint16_t CRC_t ;



#define BEEHIVE_PROTOCOL_VERSION 0x0001 // format is 2-bytes major 2-bytes minor

#define BEEHIVE_PROTOCOL_UDP_PORT 10230



enum serverErrorCode_e
{
	success					= 0x00,
	undefinedError			= 0x01,

} ;



typedef struct
{
	frameSize_t			frameSize			;
	serviceId_t			serviceId			;
	beehiveId_t			beehiveId			;
	CRC_t				CRC					;

} beehiveReadDataRequest_t ;



typedef struct
{
	frameSize_t			frameSize			;
	serviceId_t			serviceId			;
	beehiveId_t			beehiveId			;
	value_t				gaugeNo1Value		;
	value_t				gaugeNo1RawValue	;
	value_t				gaugeNo2Value		;
	value_t				gaugeNo2RawValue	;
	value_t				gaugeNo3Value		;
	value_t				gaugeNo3RawValue	;
	value_t				scaleValue			;
	value_t				scaleRawValue		;
	CRC_t				CRC ;

} beehiveReadDataResponse_t ;



typedef struct
{
	frameSize_t			frameSize			; // 1 byte
	serviceId_t			serviceId			; // 1 byte
	uint8_t				reserved_padding_1	; // 1 byte
	uint8_t				reserved_padding_2	; // 1 byte

	collectorId_t		collectorId			; // 4 bytes

	protocolVersion_t	protocolVersion		; // 2 bytes
	CRC_t				CRC					; // 2 bytes
	//                                        ---------
	//                                          12 bytes
} serverLogOnRequest_t ;



typedef struct
{
	frameSize_t			frameSize			; // 1 byte
	serviceId_t			serviceId			; // 1 byte
	uint8_t				reserved_padding_1	; // 1 byte
	uint8_t				reserved_padding_2	; // 1 byte

	sessionKey_t		sessionKey			; // 4 bytes

	errorCode_t			errorCode			; // 1 byte
	uint8_t				reserved_padding_3	; // 1 byte
	CRC_t				CRC					; // 2 bytes
	//                                        ---------
	//                                          12 bytes
} serverLogOnResponse_t ;



typedef struct
{
	frameSize_t			frameSize			; // 1 byte
	serviceId_t			serviceId			; // 1 byte
	uint8_t				reserved_padding_1	; // 1 byte
	uint8_t				reserved_padding_2	; // 1 byte

	sessionKey_t		sessionKey			; // 4 byte

	beehiveId_t			beehiveId			; // 4 byte

	value_t				gaugeNo1Value		; // 2 byte
	value_t				gaugeNo1RawValue	; // 2 byte

	value_t				gaugeNo2Value		; // 2 byte
	value_t				gaugeNo2RawValue	; // 2 byte

	value_t				gaugeNo3Value		; // 2 byte
	value_t				gaugeNo3RawValue	; // 2 byte

	value_t				scaleValue			; // 2 byte
	value_t				scaleRawValue		; // 2 byte

	uint8_t				reserved_padding_3	; // 1 byte
	uint8_t				reserved_padding_4	; // 1 byte
	CRC_t				CRC					; // 2 byte
	//                                        ---------
	//                                          32 bytes
} beehiveDataFrameRequest_t ;



typedef struct
{
	frameSize_t			frameSize			; // 1 byte
	serviceId_t			serviceId			; // 1 byte
	uint8_t				reserved_padding_1	; // 1 byte
	uint8_t				reserved_padding_2	; // 1 byte

	sessionKey_t		sessionKey			; // 4 byte

	errorCode_t			errorCode			; // 1 byte
	uint8_t				reserved_padding_3	; // 1 byte
	CRC_t				CRC					; // 2 byte
	//                                        ---------
	//                                          12 bytes
} beehiveDataFrameResponse_t ;



typedef struct
{
	frameSize_t			frameSize			; // 1 byte
	serviceId_t			serviceId			; // 1 byte
	uint8_t				reserved_padding_1	; // 1 byte
	uint8_t				reserved_padding_2	; // 1 byte

	sessionKey_t		sessionKey			; // 4 byte

	uint8_t				reserved_padding_3	; // 1 byte
	uint8_t				reserved_padding_4	; // 1 byte
	CRC_t				CRC					; // 2 byte
	//                                        ---------
	//                                          12 bytes
} serverLogOffRequest_t ;



typedef struct
{
	frameSize_t			frameSize			; // 1 byte
	serviceId_t			serviceId			; // 1 byte
	uint8_t				reserved_padding_1	; // 1 byte
	uint8_t				reserved_padding_2	; // 1 byte

	sessionKey_t		sessionKey			; // 4 byte

	errorCode_t			errorCode			; // 1 byte
	uint8_t				reserved_padding_3	; // 1 byte
	CRC_t				CRC					; // 2 byte
	//                                        ---------
	//                                          12 bytes
} serverLogOffResponse_t ;



#define SERVER_REQUEST_SIZE MAX3(sizeof(serverLogOnRequest_t),sizeof(beehiveDataFrameRequest_t),sizeof(serverLogOffRequest_t))

#define SERVER_RESPONSE_SIZE MAX3(sizeof(serverLogOnResponse_t),sizeof(beehiveDataFrameResponse_t),sizeof(serverLogOffResponse_t))



#endif /* INCLUDE_BEEHIVEFIELDPROTOCOL_H_ */
